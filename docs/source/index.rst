.. mpsf documentation master file, created by
   sphinx-quickstart on Fri Mar 18 20:28:36 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mpsf's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   README
   modules


.. code-block:: python
   :caption: Quick use

   from mpsf import smart_multi_proc

   def my_func(x):
      print(x)

   data = (i for i in range(10)) # dummy data as iterator
   smart_multi_proc(my_func, data)


`gitlab repo link  <https://gitlab.com/Yggdrasil27/mpsf>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
