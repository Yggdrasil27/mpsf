from mpsf.mpsf import DataParser

data_length = 5
data = DataParser((i for i in [2, 3]), data_length)
for i in data:
    print(i)