import pytest
from mpsf.mpsf import DataParser, smart_multi_proc
from inspect import signature


class TestDataParser:
    columns = {'data': 'Iterable', 'data_length': 'int'}

    def test_arguement_count(self):
        sig = signature(DataParser)
        assert len(sig.parameters) == len(self.columns.keys())
        for col in self.columns.keys():
            assert col in sig.parameters.keys()

    def test_arguement_type(self):
        sig = signature(DataParser)
        for col, col_type in self.columns.items():
            assert sig.parameters[col].annotation == col_type

    def test_counter(self):
        data_length = 5
        data = iter(DataParser((i for i in range(data_length)), data_length))
        for i in range(data_length):
            next(data)
        with pytest.raises(StopIteration):
            next(data)


class TestSmartMultiProc:
    def test_data_points_w_larger_max_queue_size(self):
        data_points = 10
        max_queue_size = 20
        data = (i for i in range(data_points))
        smart_multi_proc(lambda x: x, data, 1, max_queue_size=max_queue_size)

    def test_data_points_w_smaller_max_queue_size(self):
        data_points = 20
        max_queue_size = 10
        data = (i for i in range(data_points))
        smart_multi_proc(lambda x: x, data, 1, max_queue_size=max_queue_size)



# test_small_queue
